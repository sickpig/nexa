```
savetxpool

Dumps the txpool to disk.

Examples:
> nexa-cli savetxpool 
> curl --user myusername --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "savetxpool", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:7227/

```
