Release Notes for Nexa 1.4.1.0
======================================================

Nexa version 1.4.1.0  is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at gitlab:

  <https://gitlab.com/nexa/nexa/-/issues>

This is a major Nexa release, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.4.1.0
-----------------------

This is list of the main changes that have been merged in this release:

- Set protocol upgrade activation on March 15th 2025
- Implement script machine registers: OP_STORE ,  OP_LOAD (/doc/script_registers.md)
- OP_PARSE (/doc/op_parse.md)
- Add extended introspection opcodes: OP_INPUTTYPE, OP_OUTPUTTYPE, OP_INPUTVALUE (/doc/fork1.md)
- Implement negative indexes for OP_ROLL/OP_PICK (/doc/negativeRollAndPick.md)
- Read-only inputs (/doc/read-only-inputs.md)
- Script Machine limits changed (/doc/nexa-script-machine.md).
- Legacy (non-script template) outputs formats are tightly constrained
- Add support for architectures (eg RISC-V) and OS (Alpine)
- Massively code performance improvemienets for the mempool (tx acceptance, tx removal)
- Upgrade rostrum to version 10.1.0
- Improve start up time (massively improve loading of the block index)
- Various improvements to the request manager
- Significant improvements to threads locking
- New RPC documentation (https://doc.rpc.nexa.org/)
- P2P request/response message to provide a list of headers that form an ancestor path between 2 block heights

Commit details
--------------

- `ba9cf737d` Bump nexad version to 1.4.1.0 (Andrea Suisani)
- `68479f2b2` Fix script_tests.cpp compile warning (Peter Tschipper)
- `b276c374a` Tidy up -txindex startup (Peter Tschipper)
- `fcd6cdd11` Fix nexa upgrade where token reindex is initiated. (Peter Tschipper)
- `7db81290f` Fork1 Constraints (Andrew Stone)
- `14fdbb4b1` add ubuntu 24 to CI (Griffith)
- `b38075210` Only allow tokens to be sent to a p2pkt address (Peter Tschipper)
- `505caf221` Move Fork1 forktime to March 15, 2025 (Peter Tschipper)
- `4eed6f598` [build] Add support for RISC-V architectures (Andrea Suisani)
- `0f182ae92` Remove section in the FD_SET selection where we skip sending (Peter Tschipper)
- `187222c71` Instant transactions needs to filter out readonly input chains. (Peter Tschipper)
- `c16fe6875` Implement script machine registers (Griffith)
- `90435e827` Replace 3 indices in the mempool with unsorted ones (Peter Tschipper)
- `5c5de1d60` Make sure we don't increment and iterator before processing the inventory (Peter Tschipper)
- `eb776a0dd` add extended introspection opcodes: OP_INPUTTYPE, OP_OUTPUTTYPE, OP_INPUTVALUE (Andrew Stone)
- `c8317abde` include boost header files so libnexa builds properly.  Bump versions of boost and sqlite (Andrew Stone)
- `58418de09` Fix rare -addnode bug which causes cpu to spin (Peter Tschipper)
- `7f87ac3ee` Initial sync was not starting when -reindex was also set (Peter Tschipper)
- `f4f9a6e4a` Add missing WRITELOCK on setBlockIndexCandidates (Peter Tschipper)
- `d8c59b630` properly parse hex strings and int strings in token subgroup rpc (Griffith)
- `5d0604d91` Bypass intitiaizing the chain in threadimport if we're doing a sync (Peter Tschipper)
- `ddb7a4186` Properly account for Credits when parsing token transactions (Peter Tschipper)
- `dfdd35141` update readme (vgrunner)
- `9808c068b` add documentation to build on alpine (vgrunner)
- `1d1f22c97` fix rostrum.h build on alpine (vgrunner)
- `bd29c9c65` op_parse (Andrew Stone)
- `b65e35d1f` [doc] remove documentation about travis CI suite (Andrea Suisani)
- `ad98fc638` Improve performance of removeForBlock() (Peter Tschipper)
- `adacc7db5` Cleanup net code where we make outbound connections (Peter Tschipper)
- `b7e61421e` Block index upgrade (Peter Tschipper)
- `461fde260` Improve performance of txadmission in regards to readonly inputs (Peter Tschipper)
- `b3af592d8` Fix potential out of sync data for nLastBlockFile (Peter Tschipper)
- `977633879` [bash only] add files to run qa in python venv (Griffith)
- `474e13474` Add missing sync_blocks to readonlyinputs.py (Peter Tschipper)
- `36fab74c7` Always start rostrum along with nexad but not on regtest (Andrea Suisani)
- `780cc4382` in txpool, clear out dependencies chain of readonly dependencies when we have to remove one.  Also fix some ToString() functions that had errors due to disuse.  Also pull in 2 fixes from ptschip that solve ancestor counts (Andrew Stone)
- `f839eb739` only take write lock when rehashing std containers (Andrew Stone)
- `0bcfd6ab5` map rehash requires a write lock for unordered_map (Andrew Stone)
- `0ccd620fb` [gitian] Update instructions to build deterministic binaries (Andrea Suisani)
- `14705f0d0` The wallet crosschecks its balance calculation in debug builds, but txpool admission is not halted.  This means that transactions can become trusted during the calculation.  This breaks the crosscheck.  But this is ok for the actual wallet balance, because the wallets balance when active tx are being admitted can be either the pre-admission or post-admission value.  So if we find an inconsistency, check again without checking trusted status and only error if the untrusted balances are inconsistent (Andrew Stone)
- `12971a435` [build] update boost to 1.86.0 (Andrea Suisani)
- `feccfa95f` Update the dependecies.md file for boost versions (Peter Tschipper)
- `335dc16f0` Bump minimum boost library to 1.68.0 (Peter Tschipper)
- `add265bf7` Revert to boost locking (Peter Tschipper)
- `ee7bdaec5` Read only inputs (Griffith)
- `eec8a04ad` Pin rostrum electrum server to v10.1.0 (Andrea Suisani)
- `a5494301c` View NFT's using the qt wallet by accessing explorer.nexa.org (Peter Tschipper)
- `38a2c3b44` Make it possible to build rpc documentation via mkdocs (Andrea Suisani)
- `0fec0e4c9` Init the genesis before loading the block index. (Peter Tschipper)
- `ebe2538f1` Add man pages generator to the nexad repository (Andrea Suisani)
- `07244ffa4` Add documentation for nexad rpc commands (Andrea Suisani)
- `2282127c9` Aquire network connections much faster (Peter Tschipper)
- `37d5078f6` Massively improve loading of the block index (Peter Tschipper)
- `ce218d9b0` Change where we set the the token sync flag (Peter Tschipper)
- `e683caf7a` [build] Update rust package version in the depends tree (Andrea Suisani)
- `089ef05c8` Add disable wallet ci test (Griffith)
- `b6451555a` [libnexa only] Add additional helper functions around schnorr signatures (Andrew Stone)
- `a9809c541` Reduce the amount of time spent in abs tests (Peter Tschipper)
- `7fa04d2e6` [fixes blockchain.py spurios failures] fix deep chain reorg issue (Peter Tschipper)
- `59a4911fa` implement negative indexes for OP_ROLL and OP_PICK (Griffith)
- `4a8a9011f` Remove annoying second progess pcnt lablel in modaloverlay (Peter Tschipper)
- `78945a246` add privkey to pubkey benchmark (EC generator) (Andrew Stone)
- `3c4e37215` Fix spurious failures in blockchain.py (Peter Tschipper)
- `bab05946b` Add new message type TOKENINFO (Peter Tschipper)
- `86a02cbe9` Comment out a couple of verbose print statments (Peter Tschipper)
- `4db6bc755` cstddef is the exact canonical location of size_t (Andrew Stone)
- `b957a482a` Send a reject message to light clients if the transaction's inputs cannot be resolved (Andrew Stone)
- `6ae8d641d` Forking changes for multisig (Peter Tschipper)
- `f19ad60e5` Reduce the amount of time we need the CORRAL when committing to txpool (Peter Tschipper)
- `fed60641c` Use unordered maps for various lookup maps in CTxMemPool (Peter Tschipper)
- `500b9c161` Add some missing libnexa errors and setting to no error on success (Griffith)
- `57c294ab6` Create a new tweak for allowing an instant transaction cutoff price in NEX (Peter Tschipper)
- `3a0d47f78` Optimize the processing of the txDeferQ (Peter Tschipper)
- `0c139e6c6` No need to take the lock on cs_priorityRecvQ if the delay queue is empty (Peter Tschipper)
- `093916b1d` the native implementation used to call java's secureRandom because the full node used openSSL (not included in libnexa) for secure random number generation.  However, openSSL was replaced a long time ago with a native implementation based on /dev/urandom etc.  At the same time, calling from the native out to java has always been an esoteric thing to do, and recently its causing failures on many platforms according to the google play automated testing.  So remove the libnexa call out to java for secure random numbers, and replace with the same random generation used in the full node wallet. (Andrew Stone)
- `2854d56db` generate just one shared library (Andrew Stone)
- `cea87b4d6` add an api to parse group token opreturn descriptions to cashlib (Griffith)
- `25d28245e` Fuzz testing update (Andrew Stone)
- `9a05c25fd` Implement total script stack size limit (Andrew Stone)
- `fa7a2a4a9` Fix a few timing issues in abandonconflict.py (Peter Tschipper)
- `dbb9d2636` Switch to global flags instead of using GetBoolArg() (Peter Tschipper)
- `d9b1c3c14` Move token updates to after the point where we set the new tip (Peter Tschipper)
- `47008e90b` Add additional parameter handling when setting walletpassphrase (Peter Tschipper)
- `daeddcfad` Remove unused snapshot code (Peter Tschipper)
- `c247f1ca0` Change requestCookie to uint32_t (Peter Tschipper)
- `95c44ba0c` Streamline header download (Peter Tschipper)
- `19a765102` Translation file updates. (Peter Tschipper)
- `552ec84c6` Fix  improper locking (Peter Tschipper)
- `2d14ca128` Use memset() when clearing the rolling fastfilter data (Peter Tschipper)
- `dbadbfc6d` Make sure to send getdata with cookie in requestmanager (Peter Tschipper)
- `9e9c2f69f` Reduce the scope of the CORRAL when we commit txns to the mempool (Peter Tschipper)
- `fb82b878e` No need to calculate mempool ancestors if there are none (Peter Tschipper)
- `64381065d` Simplify fork1 activation helper code paths (Andrea Suisani)
- `c3649ffe4` A couple of cleanups - pass by reference (Peter Tschipper)
- `aed91a1d4` Provide libnexa access to key capd functions (Andrew Stone)
- `c4748280e` Don't use the same lock for the commitQ map and the commitQ condition variable (Peter Tschipper)
- `7030ca9aa` Use Logging::LogGetAllString() to compose nexad `-debug` help text (Andrea Suisani)
- `6c7a376a2` Check whether the input coins spends a coinbase when we get priority (Peter Tschipper)
- `0d1deb067` Improve post block processing performance - do SyncWithWallets() in a different thread (Peter Tschipper)
- `2d0b5d265` fix ubuntu24 compilation warnings and errors (no qt) (Griffith)
- `fa1a4dce8` Remove constraint on maximum stack width at an upgrade (hard fork) date (Andrew Stone)
- `f6bcc0d96` Change LOGA ot LOG statment when processing for orphans (Peter Tschipper)
- `313de5612` remove unnecessary time sleep (Peter Tschipper)
- `70636c36e` use a global waitTime of 60 seconds in mempool_accept.py (Peter Tschipper)
- `e41018af2` Fix spurious failures in mempool_accept.py (Peter Tschipper)
- `9db40d9d0` Add make-base-vm.log to the artifacts set (Andrea Suisani)
- `be80ef63d` Fix spurious failure in nodehandling.py (Peter Tschipper)
- `188073fba` Fix a few spurios failures in blockchain.py (Peter Tschipper)
- `6a3ede364` When making a new block template only perform GetMemPoolAncestors() (Peter Tschipper)
- `7096589dd` Fix spurious failure in wallet.py (Peter Tschipper)
- `c36cebd18` Add clang-format 17.0.6 to the list of tested one (Andrea Suisani)
- `6ae0ab1ad` Use boolean rather than bitwise operators (Andrea Suisani)
- `ed4942159` Fix building on FreeBSD and update the documentation (Andrew Kallmeyer)
- `31116cdf8` Process orphans in the commit tx thread rather than the block processing thread (Peter Tschipper)
- `13ac5c516` Small perf fixes to minerpolicy (Peter Tschipper)
- `bcc63016a` Change setMerkleBranch() to setBlockChainPos() (Peter Tschipper)
- `9ffd94375` A few performance improvements for getblocktemplate (Peter Tschipper)
- `eb7411837` have getdata block invs return something (notfound or reject) on error.  Silently dropping a reply makes clients time out and assume that the node is dead or slow (Andrew Stone)
- `c55cf880f` Remove priority from CUnknownObj (Peter Tschipper)
- `1c3072642` Compile Nexa for ubuntu 23.10 (Dolaned)
- `5e5435cc0` Prevent having to re-aquire the mempool locks when applying deltas (Peter TSchipper)
- `75374da18` Remove unnecessary cs_main lock in transaction description dialog (Peter Tschipper)
- `b7a888ad0` Make various improvements to the request manager (Peter Tschipper)
- `bef257b32` New INV type with uint8_t message type size (Peter Tschipper)
- `9ac237ebe` Add Product Hunt badge to README.md (Jørgen Svennevik Notland)
- `47db15fbd` Rolling Fast Filter changes (Peter Tschipper)
- `476d643f3` Relax Berkeley DB version restriction when using `--with-incompatible-bdb` configure flag (Griffith)
- `017c65908` bump minimum qt requirements to 5.9 (Griffith)
- `59c57455d` Revert horizontalAdvance() to width() for QT so we are backwardly compible (Peter Tschipper)
- `2417e5035` Update deprecated to fix compile warnings (qt) (Proteus)
- `7eedb1d13` add two templates that allow people to easily use the python test framework to access mainnet (Andrew Stone)
- `4033d747a` bump sqlite and boost versions, use curl instead of wget to access files, update one cashlib->libnexa doc, use rostrum latest (daily dev) (Andrew Stone)
- `5e39c2d9c` Reduce the scope for the LOCK for nAvgBlkResponseTime (Peter TSchipper)
- `3b79f40e2` Fix compilation errors on native windows for RecoverCompact() (Peter Tschipper)
- `fd93fe835` Rename cashlib to  libnexa (Griffith)
- `9c0e044d5` Cleanup use of variables that are not properly locked and switch to atomics (Peter Tschipper)
- `8e8633db7` Use TxMayAlreadyHave()  when checking to ask for new txns (Peter Tschipper)
- `98875f91e` net cleanup (Peter Tschipper)
- `1a1e1cf0c` Reduce locking contention on cs_objDownloader (Peter Tschipper)
- `819f719ce` show both txid and txidem in transaction details, right click menu (Griffith)
- `62e1f468c` allow message verification from DER sigs made with compressed pubkeys (Griffith)
- `550ad8846` Print the JSON rpc error if there is one when waitFor() hits the timeout limit (Peter TSchipper)
- `f534cb747` In util.py, waitFor() was not calling the function within the try/catch block (Peter TSchipper)
- `062346559` Add a few sync_blocks() to grouptokens.py (Peter TSchipper)
- `2a1cef11d` Add more details to logging in case of failure during wallet recovery procedure (Andrea Suisani)
- `881eb44a4` Create a lock and swap container for reducing lock contention (Peter Tschipper)
- `e3ad71995` clean up header path test.  make a long test option for manual testing (Andrew Stone)
- `481e5392a` fix definition of header path (Andrew Stone)
- `c1cb8d36c` add gethdrpath and hdrpath p2p node messages that request and provide a list of headers that reference eachother from one point in the blockchain to another (Andrew Stone)
- `e68bcfaae` remove unnecessary log (Andrew Stone)
- `20d2d31f3` Prevent lock contention in TxAlreadyHave() (Peter Tschipper)
- `60d39fedb` Add message cookies in place of the unused checksum field.  A message cookie is returned back to the sender in associated replies which allows a single communications channel to be used simultaneously by multiple threads without ambiguity as to where a reply should go. (Andrew Stone)
- `8b64f6524` Remove the periodic flush check from txadmission (Peter Tschipper)
- `1cf66b9ae` [1.4.1.0] Implement token whitelist (Griffith)
- `cf55f0b4d` Make relay priority a global. (Peter Tschipper)
- `5aa850cab` Skip sending socket data if the receive buffer is full (Peter TSchipper)
- `00bd3a14a` Do not avoid reading in socket data when we also want to send (Peter TSchipper)
- `744fc724d` cs_main locking cleanup (Peter Tschipper)
- `8aa6b3527` Remove Coin Freeze code (Peter Tschipper)
- `a26fcd09f` fix mininode test - header saved was actually a block (Peter TSchipper)
- `51acc84f3` Swtich to using strings for reporting of tps and peak tps (Peter Tschipper)
- `d3d3bf7db` fix typo + misleading description (dumptokenset) (Proteus)
- `5212d767a` [Explorer] add dumptokenset rpc, returns groupids of every token (Griffith)
- `4ff010ff0` Calculate the number of txns we commit per round dynamically (Peter Tschipper)
- `1b7f725a1` Add an orphanQ to txadmission (Peter Tschipper)
- `7d675deb7` Fix bug in txpool sizing when using cache.maxTxPool (Peter Tschipper)
- `210009ec6` openssl removes ripemd160 in v3.0+ (ubuntu22+), use python implementation instead (Griffith)
- `3068e91e7` Fix token display issue in Token history and Transaction history (Peter Tschipper)
- `b9a65741e` Remove ban from inventory handling if sendsize to big (Peter TSchipper)
- `fd47c2cd0` remove multisig from ERR_SIG_NULLFAIL, use ERR_MULTISIG_NULLFAIL instead (Griffith)
- `f91b6b26c` return the error from verifymessage instead of only false (Griffith)
- `3f159b80c` Tidy up SetRecvVersion (Peter Tschipper)
- `566158cc1` Remove the redunant CheckInputs() in txadmission.cpp (Peter TSchipper)
- `84eee8147` Reduce locking for peertablemodel - make nSendBytes and nREcvBytes atomics (Peter Tschipper)
- `4f60cca70` Fix performance issue in the Wallet Model when updating balances (Peter Tschipper)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Kallmeyer
- Andrew Stone
- Dolaned
- Griffith
- Jørgen Svennevik Notland
- Peter Tschipper
- Proteus
- vgrunner

